from constants import *
from utils import *


class HDM4:
    """
    Clase con los modelos de rendimiento de combustible
    """
    def __init__(self, v, c):
        """
        :param v: velocidad del vehiculo en km/h
        :param c: carga del vehiculo en kg
        """
        self.carga = c
        self.velocidad = kmh_to_ms(v)
        self.__fa = 0.7331 * Constants.CD * Constants.AF * self.velocidad ** 2
        self.__fr = (Constants.M + c) * 9.8 * (Constants.CO + Constants.CV * self.velocidad ** Constants.CM)
        self.__Ftr = self.__fa + self.__fr
        self.__Ptr = (self.velocidad * self.__Ftr) / 1000
        if self.velocidad <= 5.6:
            self.__rpm = Constants.A0 + 5.6 * Constants.A1 + 31.36 * Constants.A2
        elif self.velocidad > 5.6 and self.velocidad <= Constants.A3:
            self.__rpm = Constants.A0 + Constants.A1 * self.velocidad + Constants.A2 * self.velocidad ** 2
        else:
            self.__rpm = ((Constants.A0 + Constants.A1 * Constants.A3 + Constants.A2 * Constants.A3 ** 2) * self.velocidad) / Constants.A3
        self.__Pacs = Constants.EALC * (self.__rpm / Constants.TRPM) + Constants.ECFLC * Constants.Pmax * (self.__rpm / Constants.TRPM) ** 2.5
        self.__Peng = Constants.ceng + Constants.beng * (self.__rpm / 1000) ** 2
        self.__Ptot = self.__Ptr/Constants.EDT + self.__Pacs + self.__Peng

    def get_IFC(self):
        return self.__Ptot * Constants.BETA

    def get_ptr(self):
        return self.__Ptr

    def get_pacs(self):
        return self.__Pacs

    def get_peng(self):
        return self.__Peng

    def get_kml(self):
        segundos = 1000 / (self.__Ptot * Constants.BETA)
        return (segundos * self.velocidad) / 1000
