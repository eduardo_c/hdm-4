from utils import *


class Constants:
    """
    Clase con los parametros del modelo HDM-4 para un camion pesado
    La masa del vehiculo y su potencia son tomados de hoja de datos del modelo T800 de Kenworth
    El area frontal es tomado de una tesis
    """
    ALFA = 0.37
    BETA = 0.07
    EDT = 0.86
    TRPM = 2800
    EALC = 15
    ECFLC = 0.05
    IDLE_RPM = 500
    A0 = 1167
    A1 = -24
    A2 = 1.76
    A3 = 22
    M = 8682
    CD = 0.85
    AF = 9
    CO = 0.0041
    CV = 0.000025
    CM = 1
    Pmax = hp_to_kw(500)
    ceng = 0.017 * Pmax
    beng = 0.7 + 0.026 * Pmax

    def __init__(self):
        pass
