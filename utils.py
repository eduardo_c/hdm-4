
def kmh_to_ms(v):
    """
    convierte velocidad en km/h a m/s
    :param v: velocity in km/h
    :return: velocity in m/s
    """
    return v * 0.277778


def hp_to_kw(p):
    return p * 0.7457
